#include "Arduino.h"
#include "ultrasonicdriver.h"

ultrasonic::ultrasonic(byte pin){
  _pin=pin;
}

unsigned int ultrasonic::Current(){
unsigned int duration=0;
unsigned int lastduration=0;
  
  pinMode(_pin, OUTPUT);
  digitalWrite(_pin, LOW);
  delayMicroseconds(2);
  digitalWrite(_pin, HIGH);
  delayMicroseconds(5);
  digitalWrite(_pin, LOW);

  pinMode(_pin, INPUT);
  duration = pulseIn(_pin, HIGH, 2900);
  delay(12);
  
  if (duration<2500){ 
          if (duration-lastduration!=0){
         return duration;}}
         lastduration=duration;
}



unsigned int ultrasonic::Ave(){ 

  byte samples=3;
  unsigned int q=0;
  unsigned long durationAvarage=Current();

  for (byte i=0; i<samples; i++){
    durationAvarage= durationAvarage+ Current();
    durationAvarage= durationAvarage/2;
  }

  q = durationAvarage*3465/2/10000;
  return q;
}
