#ifndef opticalmousedriver_h
#define opticalmousedriver_h

#include "Arduino.h"



class OPin
{
	public:
		OPin(byte sda, byte scl);
		void Start();
		void Sync();
		char Read(char address);
		void Write(char address, char value);
	private:
		byte _sda;
		byte _scl;
		byte resetx;
		byte resety;
};

#endif
