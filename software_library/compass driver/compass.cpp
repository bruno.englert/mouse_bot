#include "Wire.h"
#include "Arduino.h"
#include "compass.h"


int HMC6352SlaveAddress = 0x42;
int HMC6352ReadAddress = 0x41;		//"A" in hex, A command is: "Get Data" command
int HMC6352SleepAddress = 0x53; 	//"S" in hex, S command is: "Sleep" command
int HMC6352WakeAddress = 0x57; 		//"W" in hex, W command is: "Wake" command


compass::compass(){
 int i=0;}

int compass::Angle(){


  Wire.beginTransmission(HMC6352SlaveAddress);
  Wire.write(HMC6352WakeAddress); 
  Wire.endTransmission();


  float headingSum;

  Wire.beginTransmission(HMC6352SlaveAddress);
  Wire.write(HMC6352ReadAddress);		// The "Get Data" command
  Wire.endTransmission();
  delay(8); //6000 microseconds minimum 6 ms 

  Wire.requestFrom(HMC6352SlaveAddress, 2);
  
  //"The heading output data will be the value in tenths of degrees
  //from zero to 3599 and provided in binary format over the two bytes."
  byte MSB = Wire.read();
  byte LSB = Wire.read();

  headingSum = (MSB << 8) + LSB; //(MSB / LSB sum)

  Wire.beginTransmission(HMC6352SlaveAddress);
  Wire.write(HMC6352SleepAddress); 
  Wire.endTransmission();
  
  return (headingSum / 10);
}

