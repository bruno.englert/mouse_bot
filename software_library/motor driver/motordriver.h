#ifndef motordriver_h
#define motordriver_h

#include "Arduino.h"

class MPin
{
	public:
		MPin(byte rightPinspeed, byte leftPin1,byte leftPin2,byte rightPin1,byte rightPin2,byte leftPinspeed);
		void Start();
		void Write(int speedL, int speedR);
	private:
		// Left Motor
 		  byte _leftPinspeed;
 		  byte _leftPin1;
 		  byte _leftPin2;

 		// Right Motor
		  byte _rightPinspeed;
		  byte _rightPin1;
 		  byte _rightPin2;

		////
		  int _speedL;	
		  int _speedR;			

};



#endif
