#include "Arduino.h"
#include "motordriver.h"


MPin::MPin(byte rightPinspeed, byte leftPin1,byte leftPin2,byte rightPin1,byte rightPin2,byte leftPinspeed){
   		// Left Motor
 		   _leftPinspeed=leftPinspeed;
 		   _leftPin1=leftPin1;
 		   _leftPin2=leftPin2;

 		// Right Motor
		   _rightPinspeed=rightPinspeed;
		   _rightPin1=rightPin1;
 		   _rightPin2=rightPin2;
}


void MPin::Start(){
    pinMode(_leftPin1, OUTPUT);
    pinMode(_leftPin2, OUTPUT);
    pinMode(_leftPinspeed, OUTPUT);	
    pinMode(_rightPin1, OUTPUT);	
    pinMode(_rightPin2, OUTPUT);	
    pinMode(_rightPinspeed, OUTPUT);
}


void MPin::Write(int speedR, int speedL){   //leftMotor & rightMotor should be between -255 and 255.
  _speedL=speedL;
  _speedR=speedR;

  if (speedL>=0){
    digitalWrite(_leftPin1, HIGH);
    digitalWrite(_leftPin2, LOW);
    analogWrite(_leftPinspeed, _speedL);
  } 
  else{
    digitalWrite(_leftPin1, LOW);
    digitalWrite(_leftPin2, HIGH);

    analogWrite(_leftPinspeed, -_speedL);
  }


  if (speedR>=0){
    digitalWrite(_rightPin1, HIGH);
    digitalWrite(_rightPin2, LOW);
    analogWrite(_rightPinspeed, _speedR);
  } 
  else{
    digitalWrite(_rightPin1, LOW);
    digitalWrite(_rightPin2, HIGH);
    analogWrite(_rightPinspeed, -_speedR);
  }

}



