#include "Arduino.h"
#include "opticalmousedriver.h"


OPin::OPin(byte sda, byte scl){
	_sda = sda;
	_scl = scl;
}


void OPin::Start(){
 	pinMode(_sda, OUTPUT);
	pinMode(_scl, OUTPUT);

	delay(100);
 	Sync();
 	Write(0x05, 0xA1);  // puts the mouse in 'always on' mode
	mouse.Write(0x06,0x00); //sets the mouse resolution to 800DPI

	delay(10);
        do {                      //resets the X/Y register to zero
  	  Read(0x02);
  	  resetx=Read(0x03);
 	  resety=Read(0x04);
   	} while (resety!=0 && resetx!=0);

}



void OPin::Sync(){

    digitalWrite(_scl, HIGH);
    delay(1);
	digitalWrite(_scl, LOW);
    delay(1);
	digitalWrite(_scl, HIGH);
    delay(100);
}


char OPin::Read(char address){
  char value=0;
  pinMode(_sda, OUTPUT);
  digitalWrite(_scl, HIGH); 
  address &= 0x7F; 

  for(int address_bit=7; address_bit >=0; address_bit--){
    digitalWrite(_scl, LOW); 
    pinMode(_sda, OUTPUT); 
    if(address & (1<<address_bit)){
      digitalWrite(_sda, HIGH);
    }
    else{
      digitalWrite(_sda, LOW);
    }

    delayMicroseconds(10);
    digitalWrite(_scl, HIGH);
    delayMicroseconds(10);
  }

  delayMicroseconds(120);  
  pinMode(_sda, INPUT);
  digitalWrite(_sda, HIGH);

  for(int value_bit=7; value_bit >= 0; value_bit--){
    digitalWrite(_scl, LOW);
    delayMicroseconds(10);
    digitalWrite(_scl, HIGH);
    delayMicroseconds(10); 

    if(digitalRead(_sda)){
      value |= (1<<value_bit);
    }
  }
  return value;
}	


void OPin::Write(char address, char value){
  pinMode(_sda, OUTPUT);	
  digitalWrite(_scl, HIGH);
  address |= 0x80;

  for(int address_bit=7; address_bit >=0; address_bit--){
    digitalWrite(_scl, LOW); 
    delayMicroseconds(10); 
    if(address & (1<<address_bit)){
      digitalWrite(_sda, HIGH);
    }
    else{ 
      digitalWrite(_sda, LOW);
    }

    delayMicroseconds(10);
    digitalWrite(_scl, HIGH);
    delayMicroseconds(10);
  }


  for(int value_bit=7; value_bit >= 0; value_bit--){
    digitalWrite(_scl, LOW); 
    if(value & (1<<value_bit)){
      digitalWrite(_sda, HIGH);
    }else{
      digitalWrite(_sda, LOW);
      delayMicroseconds(10);
      digitalWrite(_scl, HIGH);
      delayMicroseconds(10);
    }

  } 
}





