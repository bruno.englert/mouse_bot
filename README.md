# Mouse Bot

This poject is an arudino controlled tracked LEGO robot. It has an ultrasonic distance sensor and an optical mouse for measuring how much distance it traveled.

![Front](front.JPG)
![Left Side](left_side.JPG)
![Right Side](right_side.JPG)
![Back](back.JPG)
