#ifndef ultrasonicdriver_h
#define ultrasonicdriver_h

#include "Arduino.h"

class UPin
{
	public:
		UPin(byte pin);
		unsigned int Current();
		unsigned int Ave();
	private:
	        byte _pin;			

};



#endif
